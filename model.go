package cloudeventprovider

type ProtocolType string
type ConnectionType string

const (
	Http          ProtocolType = "http"
	Kafka         ProtocolType = "kafka"
	Nats          ProtocolType = "nats"
	NatsJetstream ProtocolType = "natsJetstream"
	Mqtt          ProtocolType = "mqtt"
	Amqp          ProtocolType = "amqp"
)

const (
	Pub ConnectionType = "pub"
	Sub ConnectionType = "sub"
	Req ConnectionType = "req"
	Rep ConnectionType = "rep"
)
