package cloudeventprovider

import (
	"errors"
	"testing"
)

func TestOptionalConfig(t *testing.T) {
	// optional config timeoutInSec is missing
	t.Setenv("CLOUDEVENTPROVIDER_MESSAGING_PROTOCOL", "nats")
	t.Setenv("CLOUDEVENTPROVIDER_MESSAGING_NATS_URL", "http://localhost:4222")
	t.Setenv("CLOUDEVENTPROVIDER_MESSAGING_NATS_SUBJECT", "events")
	t.Setenv("CLOUDEVENTPROVIDER_MESSAGING_NATS_QUEUEGROUP", "test")

	err := loadConfig()
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
}

func TestMissingConfig(t *testing.T) {
	// subject config is missing
	t.Setenv("CLOUDEVENTPROVIDER_MESSAGING_PROTOCOL", "nats")
	t.Setenv("CLOUDEVENTPROVIDER_MESSAGING_NATS_URL", "http://localhost:4222")

	err := loadConfig()
	if !errors.Is(errors.Unwrap(err), ErrConfigKeyMissing) {
		t.Errorf("Expected loadConfig to throw an ErrConfigKeyMissing but err is '%v' instead", err)
	}
}
