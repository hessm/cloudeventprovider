package cloudeventprovider

import (
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
)

var cloudEventProviderViper *viper.Viper

func init() {
	cloudEventProviderViper = viper.New()
}

type cloudEventProviderConfiguration struct {
	Messaging struct {
		Protocol ProtocolType `mapstructure:"protocol"`

		Nats struct {
			Url          string        `mapstructure:"url"`
			QueueGroup   string        `mapstructure:"queueGroup,omitempty"`
			TimeoutInSec time.Duration `mapstructure:"timeoutInSec,omitempty"`
		} `mapstructure:"nats"`

		NatsJetstream struct {
			Url          string        `mapstructure:"url"`
			QueueGroup   string        `mapstructure:"queueGroup,omitempty"`
			StreamType   string        `mapstructure:"streamType"`
			TimeoutInSec time.Duration `mapstructure:"timeoutInSec,omitempty"`
		} `mapstructure:"natsJetstream"`

		Kafka struct {
			Url      string `mapstructure:"url"`
			GroupId  string `mapstructure:"groupId,omitempty"`
			ClientId string `mapstructure:"clientId"`
		} `mapstructure:"kafka"`

		Mqtt struct {
			Url      string `mapstructure:"url"`
			ClientId string `mapstructure:"clientId"`
		} `mapstructure:"mqtt"`

		Ampq struct {
			Url      string `mapstructure:"url"`
			ClientId string `mapstructure:"clientId"`
		} `mapstructure:"amqp"`

		Http struct {
			Url  string `mapstructure:"url"`
			Port int    `mapstructure:"port"`
			Path string `mapstructure:"path"`
		} `mapstructure:"http"`
	} `mapstructure:"messaging"`
}

var currentCloudEventConfig cloudEventProviderConfiguration

func loadConfig() error {
	if err := bindEnvs(); err != nil {
		return err
	}
	readConfig()

	if err := cloudEventProviderViper.Unmarshal(&currentCloudEventConfig); err != nil {
		return err
	}
	if err := checkConfig(); err != nil {
		return err
	}
	return nil
}

func checkConfig() error {
	if cloudEventProviderViper.IsSet("messaging.protocol") {
		err := checkIfProtocolConfigIsSet(currentCloudEventConfig.Messaging.Protocol)
		if err != nil {
			return err
		}
		return nil
	} else {
		return fmt.Errorf("protocol is not set")
	}
}

func checkIfProtocolConfigIsSet(protocol ProtocolType) error {
	//TODO: loop trough config -> no need for switch

	var err error

	switch protocol {
	case Http:
		err = checkIfAllConfigKeysAreSet(protocol, currentCloudEventConfig.Messaging.Http)
	case Nats:
		//TODO: check for subject name conventions https://docs.nats.io/nats-concepts/subjects
		err = checkIfAllConfigKeysAreSet(protocol, currentCloudEventConfig.Messaging.Nats)
	case NatsJetstream:
		err = checkIfAllConfigKeysAreSet(protocol, currentCloudEventConfig.Messaging.NatsJetstream)
	case Kafka:
		err = checkIfAllConfigKeysAreSet(protocol, currentCloudEventConfig.Messaging.Kafka)
	case Mqtt:
		err = checkIfAllConfigKeysAreSet(protocol, currentCloudEventConfig.Messaging.Mqtt)
	case Amqp:
		err = checkIfAllConfigKeysAreSet(protocol, currentCloudEventConfig.Messaging.Ampq)
	default:
		err = fmt.Errorf("protocol %s is not supported", protocol)
	}
	return err
}

func checkIfAllConfigKeysAreSet(protocol ProtocolType, config interface{}) error {
	var configMap map[string]interface{}
	if err := mapstructure.Decode(config, &configMap); err != nil {
		return fmt.Errorf("could not decode config to map for checking config: %w", err)
	}

	for configKey := range configMap {
		if !cloudEventProviderViper.IsSet("messaging." + string(protocol) + "." + configKey) {
			return fmt.Errorf("%w: missing configKey %s for protocol %s", ErrConfigKeyMissing, configKey, protocol)
		}
	}
	return nil
}

func bindEnvs() error {
	envs := []string{
		"messaging.protocol",
		"messaging.nats.url",
		"messaging.nats.queueGroup",
		"messaging.nats.timeOutInSec",
		"messaging.natsJetstream.url",
		"messaging.natsJetstream.queueGroup",
		"messaging.natsJetstream.streamType",
		"messaging.natsJetstream.timeOutInSec",
		"messaging.kafka.url",
		"messaging.kafka.groupId",
		"messaging.kafka.clientId",
		"messaging.mqtt.url",
		"messaging.mqtt.clientId",
		"messaging.ampq.url",
		"messaging.ampq.clientId",
		"messaging.http.url",
		"messaging.http.port",
		"messaging.http.path",
	}

	for _, env := range envs {
		err := cloudEventProviderViper.BindEnv(env)
		if err != nil {
			return fmt.Errorf("could not bind env %s: %w", env, err)
		}
	}
	return nil
}

func readConfig() {
	cloudEventProviderViper.SetConfigName("config")
	cloudEventProviderViper.SetConfigType("yaml")
	cloudEventProviderViper.AddConfigPath(".")

	cloudEventProviderViper.SetEnvPrefix("CLOUDEVENTPROVIDER")
	cloudEventProviderViper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if err := cloudEventProviderViper.ReadInConfig(); err != nil {
		var configFileNotFoundError viper.ConfigFileNotFoundError
		if errors.As(err, &configFileNotFoundError) {
			log.Printf("Configuration not found but environment variables will be taken into account.")
		}
		cloudEventProviderViper.AutomaticEnv()
	}
}
