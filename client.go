package cloudeventprovider

import (
	"context"
	"fmt"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/cloudevents/sdk-go/v2/client"
	"github.com/cloudevents/sdk-go/v2/event"
	"github.com/cloudevents/sdk-go/v2/protocol"
)

type CloudEventProviderClient struct {
	context          context.Context
	protocol         ProtocolType
	conn             cloudEventConnection
	connectionClient client.Client
	connectionType   ConnectionType
	alive            bool
}

// newClient ignores topic parameter for http connections
func newClient(connectionType ConnectionType, topic string) (*CloudEventProviderClient, error) {
	protocol := currentCloudEventConfig.Messaging.Protocol

	ctx := context.Background()
	var connection interface{}
	var err error

	if protocol == Http {
		httpConfig := currentCloudEventConfig.Messaging.Http

		switch connectionType {
		case Pub, Req:
			connection, err = cloudevents.NewHTTP(cloudevents.WithTarget(httpConfig.Url))
		case Sub, Rep:
			connection, err = cloudevents.NewHTTP(cloudevents.WithPort(httpConfig.Port), cloudevents.WithPath("/"+httpConfig.Path))
		default:
			return nil, fmt.Errorf("unknown connectionType: %s. Could not create HttpConnection", connectionType)
		}

	} else {
		connection, err = newCloudEventConnection(protocol, connectionType, topic)
	}

	if err != nil {
		return nil, err
	}

	connectionClient, err := cloudevents.NewClient(connection)
	if err != nil {
		return nil, err
	}

	// if http cloudEventConnection = nil
	cloudEventConnection, _ := connection.(cloudEventConnection)

	newClient := &CloudEventProviderClient{
		context:          ctx,
		protocol:         protocol,
		conn:             cloudEventConnection,
		connectionClient: connectionClient,
		connectionType:   connectionType,
		alive:            true,
	}

	return newClient, nil
}

func (c *CloudEventProviderClient) Close() error {
	//TODO: what about closing http?
	if c.protocol == Http {
		return nil
	}

	if err := c.conn.Close(c.context); err != nil {
		return err
	}

	c.alive = false
	return nil
}

func (c *CloudEventProviderClient) Alive() bool {
	return c.alive
}

func (c *CloudEventProviderClient) Pub(event event.Event) error {
	if c.connectionType == Pub {
		result := c.connectionClient.Send(c.context, event)
		if err := getResultError(result); err != nil {
			return err
		}
		return nil
	} else {
		return fmt.Errorf("pub is not supported for connectionType %s", c.connectionType)
	}
}

// Sub method is blocking. Use it in a goroutine.
func (c *CloudEventProviderClient) Sub(fn func(event event.Event)) error {
	if c.connectionType == Sub {
		if err := c.connectionClient.StartReceiver(c.context, fn); err != nil {
			return err
		}
		return nil
	} else {
		return fmt.Errorf("sub is not supported for connectionType %s", c.connectionType)
	}
}

func (c *CloudEventProviderClient) Request(event event.Event, timeOut time.Duration) (*event.Event, error) {
	if c.connectionType == Req {
		ctx := context.WithValue(c.context, "timeOut", timeOut)
		response, result := c.connectionClient.Request(ctx, event)
		if err := getResultError(result); err != nil {
			return nil, err
		}
		return response, nil
	} else {
		return nil, fmt.Errorf("request is not supported for connectionType %s", c.connectionType)
	}
}

// Reply method is blocking. Use it in a goroutine.
func (c *CloudEventProviderClient) Reply(responseFunc func(ctx context.Context, event event.Event) (*event.Event, error)) error {
	if c.connectionType == Rep {
		switch c.protocol {
		case Nats:
			natsReplyConsumer, ok := c.conn.(natsReplyConsumerInterface)
			if !ok {
				return fmt.Errorf("reply is not supported for connectionType %s", c.connectionType)
			}
			if err := natsReplyConsumer.Reply(c.context, responseFunc); err != nil {
				return err
			}
			return nil
		case Http:
			err := c.connectionClient.StartReceiver(c.context, responseFunc)
			if err != nil {
				return fmt.Errorf("error while starting receiver: %w", err)
			}
			return nil
		default:
			return fmt.Errorf("reply is not supported for protocol %s", c.protocol)
		}
	} else {
		return fmt.Errorf("reply is not supported for connectionType %s", c.connectionType)
	}
}

func getResultError(result protocol.Result) error {
	if cloudevents.IsUndelivered(result) {
		return fmt.Errorf("failed to send event: %w", result)
	} else if cloudevents.IsNACK(result) {
		return fmt.Errorf("failed to publish event, event not ack: %w", result)
	} else {
		return nil
	}
}
