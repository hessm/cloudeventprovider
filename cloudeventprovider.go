package cloudeventprovider

import (
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/cloudevents/sdk-go/v2/event"
	"github.com/google/uuid"
)

func NewEvent(eventSource string, eventType string, data json.RawMessage) (event.Event, error) {
	newEvent := cloudevents.NewEvent()
	newEvent.SetID(uuid.New().String())
	newEvent.SetSource(eventSource)
	newEvent.SetType(eventType)
	if err := newEvent.SetData(cloudevents.ApplicationJSON, data); err != nil {
		return newEvent, err
	}

	return newEvent, nil
}

func NewClient(connectionType ConnectionType, topic string) (*CloudEventProviderClient, error) {
	if err := loadConfig(); err != nil {
		panic(err)
	}
	return newClient(connectionType, topic)
}
