package cloudeventprovider

import (
	"context"
	"fmt"
	"github.com/Azure/go-amqp"
	"github.com/cloudevents/sdk-go/protocol/kafka_sarama/v2"
	"github.com/nats-io/nats.go"
	"log"
	"net"
	"net/url"
	"strings"
	"time"

	"github.com/Shopify/sarama"
	ceamqp "github.com/cloudevents/sdk-go/protocol/amqp/v2"
	mqttPaho "github.com/cloudevents/sdk-go/protocol/mqtt_paho/v2"
	cenats "github.com/cloudevents/sdk-go/protocol/nats/v2"
	cejsm "github.com/cloudevents/sdk-go/protocol/nats_jetstream/v2"
	"github.com/eclipse/paho.golang/paho"
)

type cloudEventConnection interface {
	Close(ctx context.Context) error
}

func newCloudEventConnection(protocolType ProtocolType, connectionType ConnectionType, topic string) (cloudEventConnection, error) {
	var newConnection cloudEventConnection
	var err error

	switch protocolType {
	case Kafka:
		newConnection, err = newKafkaConnection(connectionType, topic)
		if err != nil {
			return nil, err
		}
	case Nats:
		newConnection, err = newNatsConnection(connectionType, topic)
		if err != nil {
			return nil, err
		}
	case NatsJetstream:
		newConnection, err = newNatsJetstreamConnection(connectionType, topic)
		if err != nil {
			return nil, err
		}
	case Mqtt:
		newConnection, err = newMqttConnection(connectionType, topic)
		if err != nil {
			return nil, err
		}
	case Amqp:
		newConnection, err = newAmqpConnection(connectionType, topic)
		if err != nil {
			return nil, err
		}
	default:
		return nil, fmt.Errorf("unknown protocolType: %s. Could not create cloudEventConnection", protocolType)
	}
	return newConnection, nil
}

func newKafkaConnection(connectionType ConnectionType, topic string) (cloudEventConnection, error) {
	kafkaConfig := currentCloudEventConfig.Messaging.Kafka

	saramaConfig := sarama.NewConfig()
	saramaConfig.Version = sarama.V2_0_0_0
	switch connectionType {
	case Pub:
		sender, err := kafka_sarama.NewSender([]string{kafkaConfig.Url}, saramaConfig, topic)
		if err != nil {
			return nil, err
		}
		return sender, nil
	case Sub:
		receiver, err := kafka_sarama.NewConsumer([]string{kafkaConfig.Url}, saramaConfig, kafkaConfig.GroupId, topic)
		if err != nil {
			return nil, err
		}
		return receiver, nil
	default:
		return nil, fmt.Errorf("unknown connectionType: %s. Could not create KafkaConnection", connectionType)
	}
}

func newNatsConnection(connectionType ConnectionType, topic string) (cloudEventConnection, error) {
	natsConfig := currentCloudEventConfig.Messaging.Nats
	var natsOptions []nats.Option
	if natsConfig.TimeoutInSec != 0*time.Second {
		natsOptions = append(natsOptions, nats.Timeout(natsConfig.TimeoutInSec*time.Second))
	}

	switch connectionType {
	case Pub:
		sender, err := cenats.NewSender(natsConfig.Url, topic, natsOptions)
		if err != nil {
			return nil, err
		}
		return sender, nil
	case Sub:
		var consumerOption cenats.ConsumerOption
		if natsConfig.QueueGroup != "" {
			consumerOption = cenats.WithQueueSubscriber(natsConfig.QueueGroup)
		}

		consumer, err := cenats.NewConsumer(natsConfig.Url, topic, natsOptions, consumerOption)
		if err != nil {
			return nil, err
		}
		return consumer, nil
	case Req:
		requester, err := newNatsRequester(natsConfig.Url, topic, natsOptions...)
		if err != nil {
			return nil, err
		}
		return requester, nil
	case Rep:
		respondConsumer, err := newNatsRespondConsumer(natsConfig.Url, topic, natsConfig.QueueGroup, natsOptions...)
		if err != nil {
			return nil, err
		}
		return respondConsumer, nil
	default:
		return nil, fmt.Errorf("unknown connectionType: %s. Could not create NatsConnection", connectionType)
	}
}

func newNatsJetstreamConnection(connectionType ConnectionType, topic string) (cloudEventConnection, error) {
	natsJetstreamConfig := currentCloudEventConfig.Messaging.NatsJetstream
	var natsJetstreamOptions []nats.Option
	if natsJetstreamConfig.TimeoutInSec != 0*time.Second {
		natsJetstreamOptions = append(natsJetstreamOptions, nats.Timeout(natsJetstreamConfig.TimeoutInSec*time.Second))
	}

	switch connectionType {
	case Pub:
		sender, err := cejsm.NewSender(natsJetstreamConfig.Url, natsJetstreamConfig.StreamType, topic, natsJetstreamOptions, nil)
		if err != nil {
			return nil, err
		}
		return sender, nil
	case Sub:
		var consumerOption cejsm.ConsumerOption
		if natsJetstreamConfig.QueueGroup != "" {
			consumerOption = cejsm.WithQueueSubscriber(natsJetstreamConfig.QueueGroup)
		}

		consumer, err := cejsm.NewConsumer(natsJetstreamConfig.Url, natsJetstreamConfig.StreamType, topic, natsJetstreamOptions, nil, nil, consumerOption)
		if err != nil {
			return nil, err
		}
		return consumer, nil
	default:
		return nil, fmt.Errorf("unknown connectionType: %s. Could not create NatsJetstreamConnection", connectionType)
	}
}

func newMqttConnection(connectionType ConnectionType, topic string) (cloudEventConnection, error) {
	mqttConfig := currentCloudEventConfig.Messaging.Mqtt
	ctx := context.Background()

	conn, err := net.Dial("tcp", mqttConfig.Url)
	if err != nil {
		return nil, err
	}

	switch connectionType {
	case Pub:
		connectionConfig := &paho.ClientConfig{
			ClientID: mqttConfig.ClientId,
			Conn:     conn,
		}
		// optional connect option
		connOpt := &paho.Connect{
			KeepAlive:  30,
			CleanStart: true,
		}

		sender, err := mqttPaho.New(ctx, connectionConfig, mqttPaho.WithPublish(&paho.Publish{Topic: topic}), mqttPaho.WithConnect(connOpt))
		if err != nil {
			return nil, err
		}
		return sender, nil
	case Sub:
		connectionConfig := &paho.ClientConfig{
			ClientID: mqttConfig.ClientId,
			Conn:     conn,
		}
		subscribeOpt := &paho.Subscribe{
			Subscriptions: []paho.SubscribeOptions{
				{
					Topic: topic,
					QoS:   0,
				},
			},
		}

		consumer, err := mqttPaho.New(ctx, connectionConfig, mqttPaho.WithSubscribe(subscribeOpt))
		if err != nil {
			return nil, err
		}
		return consumer, nil
	default:
		return nil, fmt.Errorf("unknown connectionType: %s. Could not create MqttConnection", connectionType)
	}
}

func newAmqpConnection(connectionType ConnectionType, topic string) (cloudEventConnection, error) {
	amqpUrl, node, opts := parseAmqpConfig()
	if connectionType == Sub || connectionType == Pub {
		var protocol *ceamqp.Protocol
		var err error
		if topic != "" {
			protocol, err = ceamqp.NewProtocol(amqpUrl, topic, []amqp.ConnOption{}, []amqp.SessionOption{}, opts...)
		} else {
			protocol, err = ceamqp.NewProtocol(amqpUrl, node, []amqp.ConnOption{}, []amqp.SessionOption{}, opts...)
		}
		if err != nil {
			return nil, err
		}
		return protocol, nil
	} else {
		return nil, fmt.Errorf("unknown connectionType: %s. Could not create AmqpConnection", connectionType)
	}
}

func parseAmqpConfig() (amqpUrl, node string, opts []ceamqp.Option) {
	// TODO: authentication over URL is not safe!
	amqpUrl = currentCloudEventConfig.Messaging.Ampq.Url
	parsedUrl, err := url.Parse(amqpUrl)
	if err != nil {
		log.Fatal(err)
	}
	if parsedUrl.User != nil {
		user := parsedUrl.User.Username()
		pass, _ := parsedUrl.User.Password()
		opts = append(opts, ceamqp.WithConnOpt(amqp.ConnSASLPlain(user, pass)))
	}
	return amqpUrl, strings.TrimPrefix(parsedUrl.Path, "/"), opts
}
